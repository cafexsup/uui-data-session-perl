#!/usr/bin/perl
use LWP::Simple;
use CGI qw(:standard);

print "Content-type:text/html\r\n\r\n";
use CGI qw(:standard);
my $uuid = param("uuid");

my $uri = 'http://172.31.250.45:8080/gateway/sessions/session';
my $json = '
        {
            "webAppId": "sixteencharacter",
            "allowedOrigins": ["*"],
            "urlSchemeDetails": {
                "host": "172.31.250.45",
                "port": "8443",
                "secure": true
            },
            "voice":
            {
                "username": "bob",
                "displayName": "bob",
                "domain": "172.31.250.45",
                "inboundCallingEnabled": false
            },
            "uuiData": "'.$uuid.'"
        }
    ';
my $req = HTTP::Request->new( 'POST', $uri );
$req->header( 'Content-Type' => 'application/json' );
$req->content( $json );

my $lwp = LWP::UserAgent->new;
my $res = $lwp->request( $req );

# Check the outcome of the response
if ($res->is_success) {
  print $res->content;;
} else {
  print $res->status_line, "n";
}

1;